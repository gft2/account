package com.gft.bank.account.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.gft.bank.account.RFC3339DateFormat;

import java.util.Date;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RFC3339DateFormat.class)
@WebAppConfiguration
public class RFC3339DateFormatTest {

    @InjectMocks
	private RFC3339DateFormat rfc3339DateFormat;


    @Test
    @DisplayName("Should FC3339Date Format is correct ")
    public void shouldRFC3339DateFormatisTrue(){
        Date date = new Date();
        RFC3339DateFormat dateFormat = new RFC3339DateFormat();
        //assertEquals(rfc3339DateFormat.format(date),dateFormat.format(date));
    }

}
