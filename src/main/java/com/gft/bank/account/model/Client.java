package com.gft.bank.account.model;

import java.util.List;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;



import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-04-07T21:01:13.663Z")
@Entity
@Table(name = "CLIENT")
public class Client {
	
	@JsonProperty("id")

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonProperty("first_name")
	private String first_name;
	@JsonProperty("last_name")
	private String last_name;
	@JsonProperty("email")
	private String email;
	@JsonProperty("genere")
	private String genere;
	@JsonProperty("accounts")
	

	/**
	 * Get id
	 * 
	 * @return id
	 **/
	@ApiModelProperty(example = "213", value = "")

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Get first_name
	 * 
	 * @return first_name
	 **/
	@ApiModelProperty(example = "pito", value = "")

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	/**
	 * Get last_name
	 * 
	 * @return last_name
	 **/
	@ApiModelProperty(example = "perez", value = "")

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	/**
	 * Get email
	 * 
	 * @return email
	 **/
	@ApiModelProperty(example = "loquesea@igual.com", value = "")

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Get genere
	 * 
	 * @return genere
	 **/
	@ApiModelProperty(example = "M", value = "")

	public String getGenere() {
		return genere;
	}

	public void setGenere(String genere) {
		this.genere = genere;
	}


	
	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Client client = (Client) o;
		return Objects.equals(this.id, client.id) && Objects.equals(this.first_name, client.first_name)
				&& Objects.equals(this.last_name, client.last_name) && Objects.equals(this.email, client.email)
				&& Objects.equals(this.genere, client.genere);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, first_name, last_name, email, genere);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Account {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    first_name: ").append(toIndentedString(first_name)).append("\n");
		sb.append("    last_name: ").append(toIndentedString(last_name)).append("\n");
		sb.append("    email: ").append(toIndentedString(email)).append("\n");
		sb.append("    status: ").append(toIndentedString(genere)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}




}
