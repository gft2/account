FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /target/account-bank-*.jar /tmp/app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/tmp/app.jar"]
