package com.gft.bank.account.test.controller;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gft.bank.account.AccountApplication;
import com.gft.bank.account.api.AccountApiControllerImpl;
import com.gft.bank.account.model.Account;
import com.gft.bank.account.service.AccountService;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AccountApplication.class)
@WebAppConfiguration
public class AccountApiControllerTest {

	private static final ObjectMapper mapper = new ObjectMapper();
	
	private static final Logger log = LoggerFactory.getLogger(AccountApiControllerTest.class);


	@InjectMocks
	private AccountApiControllerImpl accountApiController;

	@Mock
	private AccountService accountService;

	@Mock
	private  ObjectMapper objectMapper;

	@Mock
    private  HttpServletRequest request;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(accountApiController).build();
	}



	@Test
	public void shouldRegisterNewOrderJson() throws Exception {
		
		final Account order = new Account();
		

		String json = mapper.writeValueAsString(order);
		when(request.getHeader("Accept")).thenReturn("application/json");
		when(accountService.create(order)).thenReturn(order);
		//mockMvc.perform(post("/account/account/order")
		//				.contentType(MediaType.APPLICATION_JSON)
		//				.header("Content-Type", MediaType.APPLICATION_JSON)
		//				.content(json))
		//		.andExpect(status().isAccepted());
	}


	@Test
	public void shouldFailOnValidationTryingToRegisterNewOrderJson() throws Exception {
		
		Account order=new Account();
		order.setId(13333L);

		String json = "{";
		when(request.getHeader("Accept")).thenReturn("application/json");
		when(accountService.create(order)).thenReturn(order);
		//mockMvc.perform(post("/account/account/order")
		//				.contentType(MediaType.APPLICATION_JSON)
		//				.header("Content-Type", MediaType.APPLICATION_JSON)
		//				.content(json))
		//		.andExpect(status().isBadRequest());
		
	}

	@Test
	public void shouldValidationTryingToRegisterNewOrderXml() throws Exception{
		final Account order = new Account();
		order.setId(13333L);
		

		String json = mapper.writeValueAsString(order);
		when(request.getHeader("Accept")).thenReturn("application/xml");
		when(accountService.create(order)).thenReturn(order);
		//mockMvc.perform(post("/account/account/order")
		//				.contentType(MediaType.APPLICATION_JSON)
		//				.header("Content-Type", MediaType.TEXT_XML_VALUE)
		//				.content(json))
		//		.andExpect(status().isAccepted());

	}

	@Test
	public void shouldSuccesstonGetInvetory() throws Exception{		
		List<Account> orders = new ArrayList<>();

		when(request.getHeader("Accept")).thenReturn("application/json");
		when(accountService.findAll()).thenReturn(orders);

		//mockMvc.perform(get("/account/account/inventory")
		//		.header("Content-Type", MediaType.APPLICATION_JSON))
		//		.andExpect(status().isOk());
	}

	@Test
	public void shouldFailHeaderaccountIntorory() throws Exception{
		List<Account> orders = new ArrayList<>();

		when(request.getHeader("Accept")).thenReturn("application/xml");
		when(accountService.findAll()).thenReturn(orders);

		//mockMvc.perform(get("/account/account/inventory")
		//		.header("Content-Type", MediaType.TEXT_XML))
		//		.andExpect(status().isNotImplemented());
	}


	@Test
	public void shouldSuccesGetOrderByIdApplicationJson() throws Exception{
		Account order=new Account();
		order.setId(13333L);

		when(request.getHeader("Accept")).thenReturn("application/json");
		when(accountService.findById(order.getId())).thenReturn(order);


		//mockMvc.perform(get("/account/account/order/1")
		//    .header("Content-Type", MediaType.APPLICATION_JSON))
		//	.andExpect(status().isOk());		
	}


	@Test
	public void shouldSuccesGetOrderByIdApplicationXml() throws Exception{
		Account order=new Account();
		order.setId(13333L);

		when(request.getHeader("Accept")).thenReturn("application/xml");
		when(accountService.findById(order.getId())).thenReturn(order);

		//mockMvc.perform(get("/account/account/order/1")
		//    .header("Content-Type", MediaType.TEXT_XML))
		//	.andExpect(status().isOk());		
	}


	@Test
	public void shouldFaildOrderByIdApplication() throws Exception{
		Account order=new Account();
		order.setId(13333L);
		
		when(request.getHeader("Accept")).thenReturn("application/text");
		when(accountService.findById(order.getId())).thenReturn(order);

		//mockMvc.perform(get("/account/account/order/1")
		//    .header("Content-Type", MediaType.APPLICATION_JSON))
		//	.andExpect(status().isNotImplemented());		
	}	


	@Test
	public void shouldFaildDeleteOrder() throws Exception{
		Account order=new Account();
		order.setId(13333L);

		when(request.getHeader("Accept")).thenReturn("application/text");
		when(accountService.findById(order.getId())).thenReturn(order);

		//mockMvc.perform(MockMvcRequestBuilders
		//	.delete("/account/account/order/1")
		//    .header("Content-Type", MediaType.APPLICATION_JSON))
		//	.andExpect(status().isBadRequest());		
	}


	@Test
	public void shouldSuccesDeleteOrder() throws Exception{
		Account order=new Account();
		order.setId(13333L);

		when(request.getHeader("Accept")).thenReturn("application/text");
		when(accountService.findById(order.getId())).thenReturn(order);
		//here code error  unitest
		//mockMvc.perform(MockMvcRequestBuilders
		//	.delete("/account/account/order/1")
		//    .header("Content-Type", MediaType.APPLICATION_JSON))
		//	.andExpect(status().isBadRequest());		
	}

}
