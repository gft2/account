package com.gft.bank.account.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.gft.bank.account.api.ApiResponseMessage;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes =ApiResponseMessage.class)
@WebAppConfiguration
public class ApiResponseMessageTest {

    private static final int ERROR = 1;
    private static final int WARNING = 2;
    private static final int INFO = 3;
    private static final int OK = 4;
    private static final int TOO_BUSY = 5;
    private static final int TOO = 10;
 
    private static final String message = "ok";


    @InjectMocks
    private ApiResponseMessage apiResponseMessage;
    


    @Test
	public void shouldGetApiResponseMessageCodeWithoutParameters() {
        apiResponseMessage.setCode(ERROR);
        apiResponseMessage.setMessage(message);
        apiResponseMessage.setType(message);
        assertEquals(apiResponseMessage.getCode(),ERROR);
        assertEquals(apiResponseMessage.getMessage(), message);
        assertEquals(apiResponseMessage.getType(),message);
    }


    @Test
	public void shouldGetApiResponseMessageCodeWithParameters() {
        ApiResponseMessage api  = new ApiResponseMessage(ERROR,message);
        ApiResponseMessage api2  = new ApiResponseMessage(WARNING,message);
        ApiResponseMessage api3  = new ApiResponseMessage(INFO,message);
        ApiResponseMessage api4  = new ApiResponseMessage(OK,message);
        ApiResponseMessage api5  = new ApiResponseMessage(TOO_BUSY,message);
        ApiResponseMessage api6  = new ApiResponseMessage(TOO,message);

        assertEquals(api.getCode(), ERROR);
        assertEquals(api2.getCode(), WARNING);
        assertEquals(api3.getCode(), INFO);
        assertEquals(api4.getCode(), OK);
        assertEquals(api5.getCode(), TOO_BUSY);
        assertEquals(api6.getType(), "unknown");
    }







    



}