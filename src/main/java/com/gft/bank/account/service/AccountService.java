package com.gft.bank.account.service;

import java.util.List;

import com.gft.bank.account.model.Account;



public interface AccountService {

	/**
	 * Finds account by given name
	 *
	 * @param accountId
	 * @return found account
	 */
	Account findById(Long  Id);

	/**
	 * Checks if account with the same name already exists
	 * Invokes Auth Service user creation
	 * Creates new account with default parameters
	 *
	 * @param account
	 * @return created account
	 */
	Account create(Account account);

	/**
	 * Validates and applies incoming account updates
	 * Invokes Statistics Service update
	 *
	 * @param name
	 * @param update
	 */
	void saveChanges(Account update);

	/**
	 * 
	 * @return
	 */
	List<Account> findAll();

	void deleteAccount(Account account);


}
