package com.gft.bank.account.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Order
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-04-07T21:01:13.663Z")
@Entity
@Table(name = "ACCOUNT")
public class Account {

	@JsonProperty("id")
	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@JsonProperty("balance")
	private double balance;
	@JsonProperty("account_type")
	private String account_type;
	@JsonProperty("client_owner")
	@Column(name = "client_owner")
	private Long client_owner;

	/**
	 * Get id
	 * 
	 * @return id
	 **/
	@ApiModelProperty(value = "")
	@Valid
	public Long getId() {
		return id;
	}

	/**
	 * Get balance
	 * 
	 * @return balance
	 **/
	@ApiModelProperty(example = "0.0", required = true, value = "")
	@NotNull
	public double getBalance() {
		return balance;
	}

	/**
	 * Get account_type
	 * 
	 * @return account_type
	 **/
	@ApiModelProperty(example = "Credit Card", required = true, value = "")
	@NotNull
	public String getAccount_type() {
		return account_type;
	}

	/**
	 * Get client_owner
	 * 
	 * @return client_owner
	 **/
	@ApiModelProperty(example = "1", required = true, value = "")
	@NotNull
	public Long getClient_owner() {
		return client_owner;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void setAccount_type(String account_type) {
		this.account_type = account_type;
	}

	public void setClient_owner(Long client_owner) {
		this.client_owner = client_owner;
	}


	@Override
	public boolean equals(java.lang.Object o) {
	  if (this == o) {
		return true;
	  }
	  if (o == null || getClass() != o.getClass()) {
		return false;
	  }
	  Account pet = (Account) o;
	  return Objects.equals(this.id, pet.id) &&
		  Objects.equals(this.balance, pet.balance) &&
		  Objects.equals(this.account_type, pet.account_type) &&
		  Objects.equals(this.client_owner, pet.client_owner);
	}
  
	@Override
	public int hashCode() {
	  return Objects.hash(id, balance, account_type, client_owner);
	}
  

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Account {\n");
		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    balance: ").append(toIndentedString(balance)).append("\n");
		sb.append("    account_type: ").append(toIndentedString(account_type)).append("\n");
		sb.append("    client_owner: ").append(toIndentedString(client_owner)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}