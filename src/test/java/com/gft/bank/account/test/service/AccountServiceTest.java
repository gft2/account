package com.gft.bank.account.test.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.gft.bank.account.model.Account;
import com.gft.bank.account.repository.AccountRepository;
import com.gft.bank.account.service.AccountServiceImpl;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import org.springframework.transaction.annotation.Propagation;

@RunWith(SpringRunner.class)
public class AccountServiceTest {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@InjectMocks
	private AccountServiceImpl storeService;


	@MockBean(name = "accountRepository")
	private AccountRepository accountRepository;

	@Before
	public void setup() {
		initMocks(this);
	}

	
	@Test
	@DisplayName("Should get tostring object account")
	public void shoulGetToStingObject(){
		Account account = new Account();
		account.setId(11L);
		//assertEquals(account.toString().toCharArray().length,115);
	}

	@Test
	@DisplayName("Should is equals object")
	public void shouldIsEqualObject(){
		Account account = new Account();
		account.setId(11L);
		//assertEquals(account.equals(account),true);
	}


	@Test
	@DisplayName("Should is not equals object")
	public void shouldIsNotEqualObject(){
		Account account = new Account();
		Account account2 = new Account();
		account.setId(11L);
		//assertEquals(account.equals(account2),false);
	}

	@Test
	@DisplayName("Should get hastcode account")
	public void shouldGetHastcodeaccount(){
		Account account = new Account();
		account.setId(11L);
		//assertEquals(account.hashCode(),1213507831);
	}


	@Test
	@DisplayName("Should create account with given user")
	public void shouldCreateaccountWithGivenUser() {
		Account account = new Account();
		account.setId(11L);
		
		Account resaccount=storeService.create(account);
		//assertEquals(resaccount.getPetId(),account.getPetId());
		//assertEquals(resaccount.getId() ,account.getId());
		//assertEquals(resaccount.isComplete() ,true);

		//log.info("asotmm "+resaccount.complete(true));

		//verify(accountRepository, times(1)).save(account);
	}

	@Test
	@DisplayName("Should delete account with given user")
	public void shouldDeleteaccountWithGivenUser(){
		Account account = new Account();
		Account resaccount =storeService.create(account);
		//storeService.deleteaccount(resaccount);
		//assertEquals(resaccount.getPetId(), null);
	}


	@Test
	@DisplayName("Should get all accounts null")
	public void shouldGetallaccountsNull(){
		List<Account> stores = storeService.findAll();
		//assertEquals(stores, null);
	}

	@Test
	@DisplayName("Should get all accounts")
	public void shouldGetAllaccounts(){
		createaccountbyId();
		List<Account> stores = storeService.findAll();
        List<String> actual = Arrays.asList("a", "b", "c");
		//assertEquals(actual.size(),3);

	}

	private Account createaccountbyId() {		
		Account account = new Account();
		account.setId(11L);		
		return  storeService.create(account);
	}
}
