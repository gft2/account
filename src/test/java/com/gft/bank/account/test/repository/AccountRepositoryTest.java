package com.gft.bank.account.test.repository;

import static org.junit.Assert.assertEquals;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.gft.bank.account.AccountApplication;
import com.gft.bank.account.model.Account;
import com.gft.bank.account.repository.AccountRepository;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;



@RunWith(SpringRunner.class)
@SpringBootTest(classes = AccountApplication.class)
@WebAppConfiguration
public class AccountRepositoryTest {

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	TestRespository testRespository;


	@Test
	@DisplayName("Should find account by Nick ")
	public void shouldFindaccountByNick() {
		Account stub = getStubaccount();
		//accountRepository.save(stub);
		//Account newaccount = testRespository.add(getStubaccount());
		//Account found = accountRepository.findById(newaccount.getId()).get();
		//assertEquals(stub.getId(), found.getId());
	}

	/**
	 * 
	 * @return Create a new account with id 
	 */
	private Account getStubaccount() {
		Account account = new Account();
		account.setId(11L);		
		return account;
	}

	@Component
	static  class TestRespository{
		@Autowired
		private AccountRepository accountrRepository;

		/**
		 * @return list accounts 
		 */
		 public Iterable<Account> findall(){
			 return accountrRepository.findAll();
		 }

		 /**
		  * 
		  * @param account
		  * @return A account created
		  */
		 public Account add(Account account){
			 return accountrRepository.save(account);
		 }

		/**
		 * 
		 * @param Id from account created
		 * @return account finded 
		 */
		 public Account findById(Long Id){
			 return accountrRepository.findById(Id).get();
		 }

	} 
}
