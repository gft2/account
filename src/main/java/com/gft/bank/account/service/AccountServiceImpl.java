package com.gft.bank.account.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import com.gft.bank.account.model.Account;
import com.gft.bank.account.repository.AccountRepository;

@Service
public class AccountServiceImpl implements AccountService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private AccountRepository accountRepository;

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public Account findById(Long id) {
		Optional<Account> account = accountRepository.findById(id);
		Assert.notNull(account, "can't find account with name " + account.get().getId());
		account.get().setId(account.get().getId());
		return account.get();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Account create(Account account) {

		Account existing = accountRepository.findById(account.getId()).orElse(null);
		Assert.isNull(existing, "account already exists: " + account.getId());
		accountRepository.save(account);
		log.info("new account has been created: " + account.getId());

		return account;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveChanges(Account update) {
		Optional<Account> account = accountRepository.findById(update.getId());
		Assert.notNull(account, "can't find account with name " + update.getId());
		account.get().setId(update.getId());
		account.get().setAccount_type(update.getAccount_type());
		account.get().setBalance(update.getBalance());
		account.get().setClient_owner(update.getClient_owner());
		
		accountRepository.save(account.get());
		log.debug("account {} changes has been saved", update.getId());
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<Account> findAll() {
		Iterable<Account> findAll = accountRepository.findAll();
		List<Account> myList = new ArrayList<Account>();
		findAll.forEach(item -> myList.add(item) );
		return myList;
	}

	@Override
	public void deleteAccount(Account account) {
		accountRepository.delete(account);
	}

	

}
